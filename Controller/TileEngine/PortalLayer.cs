﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Controller.TileEngine
{
    public class PortalLayer
    {
        private Dictionary<Rectangle, Portal> portals;

        [ContentSerializer]
        public Dictionary<Rectangle, Portal> Portals
        {
            get { return portals; }
            private set { portals = value; }
        }

        public PortalLayer()
        {
            portals = new Dictionary<Rectangle, Portal>();
        }

    }
}
