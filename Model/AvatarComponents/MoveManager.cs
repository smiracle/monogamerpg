﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.AvatarComponents
{
    public static class MoveManager
    {
        private static Dictionary<string, IMove> allMoves = new Dictionary<string, IMove>();
        private static Random random = new Random();

        public static Random Random
        {
            get { return random; }
        }

        public static void FillMoves()
        {
            AddMove(new Tackle());
        }

        public static IMove GetMove(string name)
        {
            if (allMoves.ContainsKey(name))
                return (IMove)allMoves[name].Clone();

            return null;
        }

        public static void AddMove(IMove move)
        {
            if (!allMoves.ContainsKey(move.Name))
                allMoves.Add(move.Name, move);
        }

    }
}
